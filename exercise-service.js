const { Exercise } = require('./exercise-model');

async function addExercise(exercise) {
    let addedExercise = null;
    try {
        const exerciseDocument = new Exercise(exercise);
        addedExercise = await exerciseDocument.save();
    } catch (err) {
        console.log(err);
    }
    return addedExercise;
}

async function getUserExercises(userId, options) {
    let exercises = [];
    const queryWithOptions = query => {
        if (options !== undefined) {
            const { from, to, limit } = options;
            if (from !== undefined && to !== undefined) {
                query.where('date').gte(from).lte(to).sort('date');
            }
            if (limit !== undefined) {
                query.limit(limit);
            }
        }
        return query;
    };
    try {
        exercises = await queryWithOptions(Exercise.find({ userId })).exec();
    } catch (err) {
        console.log(err);
    }
    return exercises;
}

async function getUserExercisesCount(userId) {
    let count = null;
    try {
        count = Exercise.countDocuments({ userId });
    } catch (err) {
        console.log(err);
    }
    return count;
}

module.exports.addExercise = addExercise;
module.exports.getUserExercises = getUserExercises;
module.exports.getUserExercisesCount = getUserExercisesCount;