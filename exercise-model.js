const { mongoose } = require('./mongo-connection');

const exerciseSchema = mongoose.Schema({
    userId: mongoose.ObjectId,
    description: String,
    duration: Number,
    date: Date
});

module.exports.Exercise = mongoose.model('Exercise', exerciseSchema);