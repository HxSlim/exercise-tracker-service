const { mongoose } = require('./mongo-connection');

const userSchema = mongoose.Schema({
    username: String
});

module.exports.User = mongoose.model('User', userSchema);

