const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const { body, query, validationResult, matchedData, param } = require('express-validator');

require('dotenv').config();

const { addUser, getAllUsers, getUserById } = require('./user-service');
const { addExercise, getUserExercises, getUserExercisesCount } = require('./exercise-service');

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
});

app.post('/api/users', body('username').trim().notEmpty().isLength({ max: 100 }).escape(),
  (req, res, next) => {
    if (validationResult(req).isEmpty()) {
      const data = matchedData(req);
      req.username = data.username;
      next();
    } else {
      res.json({ error: 'please provide a non empty username' });
    }
  },
  async (req, res) => {
    const user = await addUser(req.username);
    if (user !== null) {
      res.json({
        username: user.username,
        _id: user._id
      });
    } else {
      res.json({ error: 'unable to add user' });
    }
  });

app.get('/api/users', async (req, res) => {
  const users = await getAllUsers();
  res.json(users.map(user => ({ username: user.username, _id: user._id })));
});

app.post('/api/users/:_id/exercises',
  param('_id').isLength({ min: 24, max: 24 }),
  body('description').trim().notEmpty().isLength({ max: 200 }).escape(),
  body('duration').isNumeric({ no_symbols: true }),
  body('date').optional({ values: 'falsy' }).isDate({ format: 'yyyy-mm-dd' }),
  (req, res, next) => {
    const inputValidationResult = validationResult(req);
    if (inputValidationResult.isEmpty()) {
      const data = matchedData(req);
      req.userId = data._id;
      req.description = data.description;
      req.duration = data.duration;
      req.date = data.date !== undefined ? new Date(data.date) : new Date();
      next();
    } else {
      res.json({ error: inputValidationResult.mapped() });
    }
  },
  async (req, res) => {
    const user = await getUserById(req.userId);
    if (user !== null) {
      const exercise = await addExercise({
        userId: user._id,
        description: req.description,
        duration: req.duration,
        date: req.date
      });
      if (exercise !== null) {
        res.json({
          username: user.username,
          description: exercise.description,
          duration: exercise.duration,
          date: exercise.date.toDateString(),
          _id: user._id
        });
      } else {
        res.json({ error: "unable to save exercise" });
      }
    } else {
      res.json({ error: "invalid user id" });
    }
  }
);

app.get('/api/users/:_id/logs', param('_id').isLength({ min: 24, max: 24 }),
  query('from').optional({ values: 'falsy' }).isDate({ format: 'yyyy-mm-dd' }),
  query('to').optional({ values: 'falsy' }).isDate({ format: 'yyyy-mm-dd' }),
  query('limit').optional({ values: 'falsy' }).isNumeric(),
  (req, res, next) => {
    const inputValidationResult = validationResult(req);
    if (inputValidationResult.isEmpty()) {
      const data = matchedData(req);
      req.userId = data._id;
      req.from = data.from;
      req.to = data.to;
      req.limit = data.limit;
      next();
    } else {
      res.json({ error: inputValidationResult.mapped() });
    }
  },
  async (req, res) => {
    const user = await getUserById(req.userId);
    if (user !== null) {
      const exercisesCount = await getUserExercisesCount(user._id);
      const exercises = await getUserExercises(user._id, {
        from: req.from,
        to: req.to,
        limit: req.limit
      });
      if (exercises !== null && exercisesCount !== null) {
        res.json({
          username: user.username,
          count: exercisesCount,
          _id: user._id,
          log: exercises.map(exercise => ({
            description: exercise.description,
            duration: exercise.duration,
            date: exercise.date.toDateString()
          }))
        });
      } else {
        res.json({ error: `unable to get logs for user id: ${req.userId}` });
      }
    } else {
      res.json({ error: "invalid user id" });
    }
  });

const listener = app.listen(process.env.PORT || 3000, () => {
  console.log('Your app is listening on port ' + listener.address().port)
});
