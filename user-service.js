const { User } = require('./user-model');

async function addUser(username) {
    let user = null;
    try {
        user = new User({ username });
        user = await user.save();
    } catch (err) {
        console.log(err);
    }
    return user;
}

async function getAllUsers() {
    let users = [];
    try {
        users = User.find({});
    } catch (err) {
        console.log(err);
    }
    return users;
}

async function getUserById(id) {
    let user = null;
    try {
        user = User.findById(id);
    } catch (err) {
        console.log(err);
    }
    return user;
}

module.exports.addUser = addUser;
module.exports.getAllUsers = getAllUsers;
module.exports.getUserById = getUserById;
